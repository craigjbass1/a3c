export default [
  {
    id: "Barcelona",
    name: "Circuit de Barcelona - Catalunya",
    short_name: "Barcelona",
    variants: [
      {
        variant_name: "2018",
        track_id: "barcelona"
      },
      {
        variant_name: "2019",
        track_id: "barcelona_2019"
      }
    ]
  },
  {
    id: "BrandsHatch",
    name: "Brands Hatch Circuit",
    short_name: "Brands Hatch",
    variants: [
      {
        variant_name: "2018",
        track_id: "brands_hatch"
      },
      {
        variant_name: "2019",
        track_id: "brands_hatch_2019"
      }
    ]
  },
  {
    id: "Hungaroring",
    name: "Hungaroring Circuit",
    short_name: "Hungaroring",
    variants: [
      {
        variant_name: "2018",
        track_id: "hungaroring"
      },
      {
        variant_name: "2019",
        track_id: "hungaroring_2019"
      }
    ]
  },
  {
    id: "Kyalami",
    name: "Kyalami Grand Prix Circuit",
    short_name: "Kyalami",
    variants: [
      {
        variant_name: "2019",
        track_id: "kyalami_2019"
      }
    ]
  },
  {
    id: "LagunaSeca",
    name: "WeatherTech Raceway Laguna Seca",
    short_name: "Laguna Seca",
    variants: [
      {
        variant_name: "2019",
        track_id: "laguna_seca_2019"
      }
    ]
  },
  {
    id: "Misano",
    name: "Misano World Circuit",
    short_name: "Misano",
    variants: [
      {
        variant_name: "2018",
        track_id: "misano"
      },
      {
        variant_name: "2019",
        track_id: "misano_2019"
      }
    ]
  },
  {
    id: "Monza",
    name: "Autodromo Nazionale Monza",
    short_name: "Monza",
    variants: [
      {
        variant_name: "2018",
        track_id: "monza"
      },
      {
        variant_name: "2019",
        track_id: "monza_2019"
      }
    ]
  },
  {
    id: "Bathurst",
    name: "Bathurst Mount Panorama Circuit",
    short_name: "Bathurst",
    variants: [
      {
        variant_name: "2019",
        track_id: "mount_panorama_2019"
      }
    ]
  },
  {
    id: "Nurburgring",
    name: "Nurburgring",
    short_name: "Nurburgring",
    variants: [
      {
        variant_name: "2018",
        track_id: "nurburgring"
      },
      {
        variant_name: "2019",
        track_id: "nurburgring_2019"
      }
    ]
  },
  {
    id: "PaulRicard",
    name: "Circuit - Paul Ricard",
    short_name: "Paul Ricard",
    variants: [
      {
        variant_name: "2018",
        track_id: "paul_ricard"
      },
      {
        variant_name: "2019",
        track_id: "paul_ricard_2019"
      }
    ]
  },
  {
    id: "Silverstone",
    name: "Silverstone",
    short_name: "Silverstone",
    variants: [
      {
        variant_name: "2018",
        track_id: "silverstone"
      },
      {
        variant_name: "2019",
        track_id: "silverstone_2019"
      }
    ]
  },
  {
    id: "Spa",
    name: "Circuit de Spa-Francorchamps",
    short_name: "Spa",
    variants: [
      {
        variant_name: "2018",
        track_id: "spa"
      },
      {
        variant_name: "2019",
        track_id: "spa_2019"
      }
    ]
  },
  {
    id: "Suzuka",
    name: "Suzuka Circuit",
    short_name: "Suzuka",
    variants: [
      {
        variant_name: "2019",
        track_id: "suzuka_2019"
      }
    ]
  },
  {
    id: "Zandvoort",
    name: "Circuit Park Zandvoort",
    short_name: "Zandvoort",
    variants: [
      {
        variant_name: "2018",
        track_id: "zandvoort"
      },
      {
        variant_name: "2019",
        track_id: "zandvoort_2019"
      }
    ]
  },
  {
    id: "Zolder",
    name: "Circuit Zolder",
    short_name: "Zolder",
    variants: [
      {
        variant_name: "2018",
        track_id: "zolder"
      },
      {
        variant_name: "2019",
        track_id: "zolder_2019"
      }
    ]
  }
]
